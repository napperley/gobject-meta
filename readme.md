# GObject Meta

A Kotlin Native library that obtains meta data on GObject based libraries. Uses the GI Repository API. The library can be used with some other Kotlin Native libraries to generate Kotlin bindings for GObject based libraries.

## Usage

1. Clone the repository
2. In the cloned repository create the KLibs by running the following:
    `./gradlew linuxKLibrary`
3. Import the generated KLibs
4. Access the meta data (top level elements) from the namespace (the GObject based library), eg:
    ````kotlin
    import org.digieng.gobject_meta.NamespaceInfo

    fun main() {
        NamespaceInfo.use(namespace = "gtk", ver = "3.0") {
            elements.forEach { e ->
                // Process the element...
            }
        }
    }
    ````

Alternatively if one only needs access to the dependencies of a namespace then the following can be done (without loading the namespace):
```kotlin
import org.digieng.gobject_meta.NamespaceInfo

fun main() {
    val dependencies = NamespaceInfo.fetchDependencies(namespace = "gtk", ver = "3.0")
    dependencies.forEach { d ->
        // Do something with the dependency.
    }
}
```

@file:Suppress("PackageName")

package org.digieng.gobject_meta.element

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import org.digieng.gobject_meta.element.top_level.createAllMemberFunctionArguments
import org.digieng.gobject_meta.element.top_level.functionFlag
import org.digieng.gobject_meta.interfaceName
import org.digieng.gobject_meta.interfaceNamespace
import org.digieng.gobject_meta.tagName

@Suppress("ArrayInDataClass")
data class MethodElement(
    override val name: String,
    val returnType: ReturnTypeElement,
    val args: Array<ArgumentElement> = emptyArray(),
    val flag: String,
    override val deprecated: Boolean
) : Element

internal fun createMethod(methodInfo: CPointer<GIFunctionInfo>): MethodElement {
    val returnTypeInfo = g_callable_info_get_return_type(methodInfo)
    val interfaceName = if (returnTypeInfo != null) interfaceName(returnTypeInfo) else ""
    val tagName = if (returnTypeInfo != null) tagName(returnTypeInfo) else ""

    val returnType = ReturnTypeElement(
        name = if (interfaceName.isNotEmpty()) interfaceName else tagName,
        namespace = if (returnTypeInfo != null) interfaceNamespace(returnTypeInfo) else "",
        nullable = g_callable_info_may_return_null(methodInfo) == TRUE
    )
    return MethodElement(
        name = g_base_info_get_name(methodInfo)?.toKString() ?: "",
        args = createAllMemberFunctionArguments(methodInfo),
        returnType = returnType,
        flag = functionFlag(methodInfo),
        deprecated = g_base_info_is_deprecated(methodInfo) == TRUE
    )
}

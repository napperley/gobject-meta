@file:Suppress("PackageName", "EXPERIMENTAL_API_USAGE")

package org.digieng.gobject_meta.element

import gir.GIFunctionInfo
import kotlinx.cinterop.CPointer
import org.digieng.gobject_meta.element.top_level.createAllMemberFunctionArguments

@Suppress("ArrayInDataClass")
data class ConstructorElement(
    val args: Array<ArgumentElement> = emptyArray()
) : Element {
    override val name: String
        get() = ""
    override val deprecated: Boolean
        get() = false
}

internal fun createConstructor(methodInfo: CPointer<GIFunctionInfo>): ConstructorElement =
    ConstructorElement(args = createAllMemberFunctionArguments(methodInfo))

@file:Suppress("PackageName")

package org.digieng.gobject_meta.element

data class ReturnTypeElement(
    override val name: String,
    val nullable: Boolean = false,
    val namespace: String
) : Element {
    override val deprecated: Boolean
        get() = false
}

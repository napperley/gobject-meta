@file:Suppress("PackageName", "EXPERIMENTAL_API_USAGE")

package org.digieng.gobject_meta.element.top_level

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.reinterpret
import kotlinx.cinterop.toKString
import org.digieng.gobject_meta.element.*

internal fun createObjectElement(objInfo: CPointer<GIObjectInfo>): ObjectElement {
    val parentInfo = g_object_info_get_parent(objInfo)
    return ObjectElement(
        name = g_base_info_get_name(objInfo)?.toKString() ?: "",
        namespace = g_base_info_get_namespace(objInfo)?.toKString() ?: "",
        deprecated = g_base_info_is_deprecated(objInfo) == TRUE,
        constructors = createAllConstructors(objInfo),
        methods = createAllMethods(objInfo.reinterpret()),
        signals = createAllSignals(objInfo),
        properties = createAllProperties(objInfo.reinterpret()),
        parentName = g_base_info_get_name(parentInfo)?.toKString() ?: "",
        parentNamespace = g_base_info_get_namespace(parentInfo)?.toKString() ?: "",
        interfaces = createInterfaces(objInfo)
    )
}

private fun createAllProperties(info: CPointer<GIObjectInfo>): Array<PropertyElement> {
    val tmp = mutableListOf<PropertyElement>()
    val totalProps = g_object_info_get_n_properties(info)
    if (totalProps > 0) {
        (0..(totalProps - 1)).forEach { pos ->
            val prop = g_object_info_get_property(info, pos)
            if (prop != null) tmp += createProperty(prop)
        }
    }
    return tmp.toTypedArray()
}

private fun createAllSignals(objInfo: CPointer<GIObjectInfo>): Array<SignalElement> {
    val tmp = mutableListOf<SignalElement>()
    val totalSignals = g_object_info_get_n_signals(objInfo)
    if (totalSignals > 0) {
        (0..(totalSignals - 1)).forEach { pos ->
            val signalInfo = g_object_info_get_signal(objInfo, pos)
            if (signalInfo != null) tmp += createSignal(signalInfo)
        }
    }
    return tmp.toTypedArray()
}

private fun createAllMethods(info: CPointer<GIObjectInfo>): Array<MethodElement> {
    val tmp = mutableListOf<MethodElement>()
    val totalMethods = g_object_info_get_n_methods(info)
    if (totalMethods > 0) {
        val end = if (totalMethods == 1) 0 else totalMethods - 1
        (0..end).forEach { pos ->
            val methodInfo = g_object_info_get_method(info, pos)
            if (methodInfo != null && g_callable_info_is_method(methodInfo) == TRUE &&
                g_function_info_get_flags(methodInfo) != GI_FUNCTION_IS_CONSTRUCTOR
            ) {
                tmp += createMethod(methodInfo)
            }
        }
    }
    return tmp.toTypedArray()
}

private fun createAllConstructors(info: CPointer<GIObjectInfo>): Array<ConstructorElement> {
    val tmp = mutableListOf<ConstructorElement>()
    val totalMethods = g_object_info_get_n_methods(info)
    if (totalMethods > 0) {
        val end = if (totalMethods == 1) 0 else totalMethods - 1
        (0..end).forEach { pos ->
            val methodInfo = g_object_info_get_method(info, pos)
            if (methodInfo != null && g_function_info_get_flags(methodInfo) == GI_FUNCTION_IS_CONSTRUCTOR) {
                tmp += createConstructor(methodInfo)
            }
        }
    }
    return tmp.toTypedArray()
}

@Suppress("ArrayInDataClass")
/** Models a GObject class. */
data class ObjectElement(
    override val name: String,
    override val namespace: String,
    override val deprecated: Boolean,
    val constructors: Array<ConstructorElement>,
    val methods: Array<MethodElement>,
    val signals: Array<SignalElement> = emptyArray(),
    val properties: Array<PropertyElement> = emptyArray(),
    val parentName: String,
    val interfaces: Array<InterfaceElement> = emptyArray(),
    val parentNamespace: String
) : TopLevelElement

private fun createInterfaces(objInfo: CPointer<GIObjectInfo>): Array<InterfaceElement> {
    val tmp = mutableListOf<InterfaceElement>()
    val totalInterfaces = g_object_info_get_n_interfaces(objInfo)
    if (totalInterfaces > 0) {
        (0..(totalInterfaces - 1)).forEach { pos ->
            val interfaceInfo = g_object_info_get_interface(objInfo, pos)
            val name = g_base_info_get_name(interfaceInfo)?.toKString() ?: ""
            val namespace = g_base_info_get_namespace(interfaceInfo)?.toKString() ?: ""
            val deprecated = g_base_info_is_deprecated(interfaceInfo) == TRUE
            tmp += InterfaceElement(name = name, namespace = namespace, deprecated = deprecated)
        }
    }
    return tmp.toTypedArray()
}
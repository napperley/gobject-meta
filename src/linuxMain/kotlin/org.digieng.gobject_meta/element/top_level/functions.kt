@file:Suppress("PackageName")

package org.digieng.gobject_meta.element.top_level

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import org.digieng.gobject_meta.*
import org.digieng.gobject_meta.element.ArgumentElement
import org.digieng.gobject_meta.element.ReturnTypeElement

@Suppress("ArrayInDataClass")
data class FunctionElement(
    override val name: String,
    override val namespace: String,
    override val deprecated: Boolean,
    val returnType: ReturnTypeElement,
    val args: Array<ArgumentElement> = emptyArray(),
    val flag: String
) : TopLevelElement

internal fun createFunction(functionInfo: CPointer<GIFunctionInfo>): FunctionElement {
    val returnTypeInfo = g_callable_info_get_return_type(functionInfo)
    val tagName = if (returnTypeInfo != null) tagName(returnTypeInfo) else ""
    val interfaceName = if (returnTypeInfo != null) interfaceName(returnTypeInfo) else ""

    val returnType = ReturnTypeElement(
        name = if (interfaceName.isNotEmpty()) interfaceName else tagName,
        namespace = if (returnTypeInfo != null) interfaceNamespace(returnTypeInfo) else "",
        nullable = g_callable_info_may_return_null(functionInfo) == TRUE
    )
    return FunctionElement(
        name = g_base_info_get_name(functionInfo)?.toKString() ?: "",
        returnType = returnType,
        namespace = g_base_info_get_namespace(functionInfo)?.toKString() ?: "",
        deprecated = g_base_info_is_deprecated(functionInfo) == TRUE,
        flag = functionFlag(functionInfo),
        args = createAllMemberFunctionArguments(info = functionInfo)
    )
}

internal fun createAllMemberFunctionArguments(
    info: CPointer<GICallableInfo>,
    signal: Boolean = false
): Array<ArgumentElement> {
    val tmp = mutableListOf<ArgumentElement>()
    val totalArguments = g_callable_info_get_n_args(info)
    if (totalArguments > 0) {
        (0..(totalArguments - 1)).forEach { pos ->
            val argInfo = g_callable_info_get_arg(info, pos)
            if (argInfo != null) tmp += createMemberFunctionArgument(argInfo)
        }
        if (signal) {
            tmp += ArgumentElement(
                name = "user_data",
                dataType = DataType(name = "gpointer"),
                nullable = true,
                optional = true
            )
        }
    }
    return tmp.toTypedArray()
}

internal fun functionFlag(functionInfo: CPointer<GIFunctionInfo>) = when (g_function_info_get_flags(functionInfo)) {
    GI_FUNCTION_IS_CONSTRUCTOR -> "Constructor"
    GI_FUNCTION_IS_GETTER -> "Getter"
    GI_FUNCTION_IS_SETTER -> "Setter"
    GI_FUNCTION_WRAPS_VFUNC -> "Virtual Function"
    GI_FUNCTION_IS_METHOD -> "Method"
    GI_FUNCTION_THROWS -> "Function (throws error)"
    else -> "Unknown"
}

private fun createMemberFunctionArgument(argInfo: CPointer<GIArgInfo>): ArgumentElement {
    val argType = g_arg_info_get_type(argInfo)
    val tag = g_type_info_get_tag(argType)
    val interfaceName = if (argType != null) interfaceName(argType) else ""
    val typeName = if (argType != null) generateTypeName(argType, tag) else ""
    val dataType = DataType(
        name = if (interfaceName.isNotEmpty()) interfaceName else typeName,
        namespace = if (argType != null) interfaceNamespace(argType) else ""
    )

    return ArgumentElement(
        name = g_base_info_get_name(argInfo)?.toKString() ?: "",
        dataType = dataType,
        optional = g_arg_info_is_optional(argInfo) == TRUE,
        ownership = argumentOwnership(argInfo),
        nullable = g_arg_info_may_be_null(argInfo) == TRUE
    )
}

private fun generateTypeName(argType: CPointer<GITypeInfo>, tag: GITypeTag): String {
    val pointer = g_type_info_is_pointer(argType) == TRUE
    return if (typeName(tag, argType) in basicDataTypes.map { it.name }) typeName(tag, argType)
    else if (!pointer) typeName(tag, argType)
    else if (pointer) "gpointer"
    else ""
}

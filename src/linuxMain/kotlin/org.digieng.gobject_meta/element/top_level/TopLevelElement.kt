@file:Suppress("PackageName")

package org.digieng.gobject_meta.element.top_level

import org.digieng.gobject_meta.element.Element

/** Represents a top level GObject element in a namespace (GObject library). */
interface TopLevelElement : Element {
    val namespace: String
}

@file:Suppress("PackageName")

package org.digieng.gobject_meta.element.top_level

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import org.digieng.gobject_meta.element.ArgumentElement
import org.digieng.gobject_meta.element.ReturnTypeElement
import org.digieng.gobject_meta.interfaceName
import org.digieng.gobject_meta.interfaceNamespace
import org.digieng.gobject_meta.tagName

@Suppress("ArrayInDataClass")
/** Models a GObject event. */
data class SignalElement(
    override val name: String,
    override val deprecated: Boolean,
    val returnType: ReturnTypeElement,
    val args: Array<ArgumentElement>,
    override val namespace: String = ""
) : TopLevelElement

internal fun createSignal(signalInfo: CPointer<GISignalInfo>): SignalElement {
    val returnTypeInfo = g_callable_info_get_return_type(signalInfo)
    val tagName = if (returnTypeInfo != null) tagName(returnTypeInfo) else ""
    val interfaceName = if (returnTypeInfo != null) interfaceName(returnTypeInfo) else ""

    val returnType = ReturnTypeElement(
        name = if (interfaceName.isNotEmpty()) interfaceName else tagName,
        namespace = if (returnTypeInfo != null) interfaceNamespace(returnTypeInfo) else "",
        nullable = g_callable_info_may_return_null(signalInfo) == TRUE
    )
    return SignalElement(
        name = g_base_info_get_name(signalInfo)?.toKString() ?: "",
        deprecated = g_base_info_is_deprecated(signalInfo) == TRUE,
        args = createAllMemberFunctionArguments(info = signalInfo, signal = true),
        returnType = returnType
    )
}

@file:Suppress("PackageName")

package org.digieng.gobject_meta.element.top_level

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import org.digieng.gobject_meta.element.ArgumentElement
import org.digieng.gobject_meta.element.ReturnTypeElement
import org.digieng.gobject_meta.tagName

@Suppress("ArrayInDataClass")
data class CallbackElement(
    override val name: String,
    override val namespace: String,
    override val deprecated: Boolean,
    val returnType: ReturnTypeElement,
    val args: Array<ArgumentElement> = emptyArray()
) : TopLevelElement

internal fun createCallback(callbackInfo: CPointer<GICallableInfo>): CallbackElement {
    val returnTypeInfo = g_callable_info_get_return_type(callbackInfo)
    val returnTypeNamespace = g_base_info_get_namespace(returnTypeInfo)?.toKString() ?: ""
    val tagName = if (returnTypeInfo != null) tagName(returnTypeInfo) else ""
    val nullable = g_callable_info_may_return_null(callbackInfo) == TRUE

    return CallbackElement(
        name = g_base_info_get_name(callbackInfo)?.toKString() ?: "",
        namespace = g_base_info_get_namespace(callbackInfo)?.toKString() ?: "",
        deprecated = g_base_info_is_deprecated(callbackInfo) == TRUE,
        args = createAllMemberFunctionArguments(info = callbackInfo),
        returnType = ReturnTypeElement(name = tagName, nullable = nullable, namespace = returnTypeNamespace)
    )
}

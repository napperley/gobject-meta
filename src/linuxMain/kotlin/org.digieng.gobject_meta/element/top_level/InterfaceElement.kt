@file:Suppress("PackageName")

package org.digieng.gobject_meta.element.top_level

import org.digieng.gobject_meta.element.MethodElement

@Suppress("ArrayInDataClass")
/** Models a GObject interface. */
data class InterfaceElement(
    override val name: String,
    override val namespace: String,
    override val deprecated: Boolean,
    val methods: Array<MethodElement> = emptyArray()
) : TopLevelElement

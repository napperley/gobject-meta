@file:Suppress("PackageName")

package org.digieng.gobject_meta.element

import org.digieng.gobject_meta.DataType
import org.digieng.gobject_meta.Ownership

data class ArgumentElement(
    override val name: String,
    val dataType: DataType,
    val optional: Boolean = false,
    val nullable: Boolean = false,
    val ownership: Ownership = Ownership.NOTHING
) : Element {
    override val deprecated: Boolean
        get() = false
}

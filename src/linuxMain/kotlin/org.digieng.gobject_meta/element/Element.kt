@file:Suppress("PackageName")

package org.digieng.gobject_meta.element

/** Represents a GObject element. */
interface Element {
    val name: String
    val deprecated: Boolean
}

@file:Suppress("PackageName")

package org.digieng.gobject_meta.element

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString
import org.digieng.gobject_meta.*

fun createProperty(propInfo: CPointer<GIPropertyInfo>): PropertyElement {
    val typeInfo = g_property_info_get_type(propInfo)
    val tagName = if (typeInfo != null) tagName(typeInfo) else ""
    val interfaceName = if (typeInfo != null) interfaceName(typeInfo) else ""

    val dataType = DataType(
        name = if (interfaceName.isNotEmpty()) interfaceName else tagName,
        namespace = if (typeInfo != null) interfaceNamespace(typeInfo) else ""
    )
    return PropertyElement(
        name = g_base_info_get_name(propInfo)?.toKString() ?: "",
        dataType = dataType,
        deprecated = g_base_info_is_deprecated(propInfo) == TRUE,
        ownership = propertyOwnership(propInfo),
        flag = propertyFlag(propInfo)
    )
}

data class PropertyElement(
    override val name: String,
    val dataType: DataType,
    override val deprecated: Boolean,
    val flag: String,
    val ownership: Ownership = Ownership.NOTHING
) : Element

private fun propertyFlag(propInfo: CPointer<GIPropertyInfo>) = when (g_property_info_get_flags(propInfo)) {
    G_PARAM_READABLE -> "Read Only"
    G_PARAM_READWRITE -> "Read/Write"
    G_PARAM_WRITABLE -> "Write Only"
    else -> "Unknown"
}

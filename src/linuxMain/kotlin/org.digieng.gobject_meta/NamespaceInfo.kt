@file:Suppress("PackageName", "EXPERIMENTAL_UNSIGNED_LITERALS")

package org.digieng.gobject_meta

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.get
import kotlinx.cinterop.toKString
import org.digieng.gobject_meta.element.top_level.TopLevelElement
import org.digieng.gobject_meta.element.top_level.createCallback
import org.digieng.gobject_meta.element.top_level.createFunction
import org.digieng.gobject_meta.element.top_level.createObjectElement

@Suppress("unused")
/** Collects information (meta data) on a namespace (GObject library). */
object NamespaceInfo {
    val namespace: String
        get() = _namespace
    private var _namespace = ""
    val ver: String
        get() = _ver
    private var _ver = ""
    private val repo = g_irepository_get_default()
    /** Top level elements that belong to the namespace. */
    val elements: MutableList<TopLevelElement> = mutableListOf()
    /** Contains a list of all the namespaces that the namespace (if loaded) depends on in order to function. */
    val dependencies: MutableList<String> = mutableListOf()
    @Suppress("ObjectPropertyName")
    private var _loaded = false
    /** Indicates if the namespace has been loaded (*true*) or not (*false*). */
    val loaded: Boolean
        get() = _loaded

    private fun createNamespacePointer(namespace: String, ver: String) = g_irepository_require(
        error = null,
        flags = 0u,
        repository = repo,
        namespace_ = namespace,
        version = ver
    )

    /**
     * Obtains all the dependencies for a [namespace] without having to load it first.
     * @param namespace The namespace (GObject library) to use.
     * @param ver The version of the namespace to use.
     * @return A empty array if there are no dependencies.
     */
    fun fetchDependencies(namespace: String, ver: String): Array<String> {
        val tmp = mutableListOf<String>()
        val namespacePtr = createNamespacePointer(namespace, ver)
        if (namespacePtr != null) {
            val arrayPtr = g_irepository_get_dependencies(repo, namespace)
            if (arrayPtr != null) {
                var pos = 0
                // Iterate over a array pointer and append strings to dependencies, until a null string is encountered.
                while (true) {
                    val strPtr = arrayPtr[pos]
                    if (strPtr == null) break
                    else dependencies += strPtr.toKString()
                    pos++
                }
            }
        }
        return tmp.toTypedArray()
    }

    /** Loads a namespace so that it can be used. */
    fun load(namespace: String, ver: String) {
        if (!loaded) {
            clearState()
            val namespacePtr = createNamespacePointer(namespace, ver)
            if (namespacePtr != null) {
                dependencies += fetchDependencies(namespace, ver)
                processAllEntries(namespace)
                _namespace = namespace
                _loaded = true
            }
        }
    }

    private fun clearState() {
        _loaded = false
        _namespace = ""
        _ver = ""
        elements.clear()
        dependencies.clear()
    }

    /** Unloads the current namespace (frees up resources) so another namespace can be loaded. */
    fun unload() {
        clearState()
    }

    /** Temporarily loads the namespace for use. */
    fun use(namespace: String, ver: String, block: () -> Unit) {
        load(namespace, ver)
        block()
        unload()
    }

    private fun processAllEntries(namespace: String) {
        val totalMetaEntries = g_irepository_get_n_infos(repo, namespace)
        if (totalMetaEntries > 0) {
            (0..(totalMetaEntries - 1)).forEach { pos ->
                val info = g_irepository_get_info(repo, namespace, pos)
                if (info != null) processEntry(info)
            }
        } else {
            elements.clear()
        }
    }

    private fun processEntry(info: CPointer<GIBaseInfo>) {
        val type = g_base_info_get_type(info)
        @Suppress("NON_EXHAUSTIVE_WHEN")
        when (type) {
            GIInfoType.GI_INFO_TYPE_FUNCTION -> elements += createFunction(info)
            GIInfoType.GI_INFO_TYPE_CALLBACK -> elements += createCallback(info)
            GIInfoType.GI_INFO_TYPE_OBJECT -> elements += createObjectElement(info)
        }
    }
}
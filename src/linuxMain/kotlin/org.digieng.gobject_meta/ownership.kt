@file:Suppress("PackageName")

package org.digieng.gobject_meta

import gir.*
import kotlinx.cinterop.CPointer

internal fun argumentOwnership(argInfo: CPointer<GIArgInfo>) = when (g_arg_info_get_ownership_transfer(argInfo)) {
    GITransfer.GI_TRANSFER_NOTHING -> Ownership.NOTHING
    GITransfer.GI_TRANSFER_EVERYTHING -> Ownership.EVERYTHING
    GITransfer.GI_TRANSFER_CONTAINER -> Ownership.CONTAINER
}

/** Represents ownership for a value/container. */
enum class Ownership {
    /** The recipient doesn't own the value. */
    NOTHING,
    /** The recipient owns the container (its reference), but not its elements (the references in the container). */
    CONTAINER,
    /** The recipient owns the container (its reference), and all its elements (the references in the container). */
    EVERYTHING
}

internal fun propertyOwnership(propInfo: CPointer<GIPropertyInfo>) =
    when (g_property_info_get_ownership_transfer(propInfo)) {
        GITransfer.GI_TRANSFER_NOTHING -> Ownership.NOTHING
        GITransfer.GI_TRANSFER_EVERYTHING -> Ownership.EVERYTHING
        GITransfer.GI_TRANSFER_CONTAINER -> Ownership.CONTAINER
    }

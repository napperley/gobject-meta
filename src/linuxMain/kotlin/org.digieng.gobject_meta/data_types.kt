@file:Suppress("PackageName", "EXPERIMENTAL_API_USAGE")

package org.digieng.gobject_meta

import gir.*
import kotlinx.cinterop.CPointer
import kotlinx.cinterop.toKString

private val gBaseTypes = setOf(
    "gpointer",
    "gboolean",
    "gint8",
    "gint16",
    "gint32",
    "gint64",
    "guint8",
    "guint16",
    "guint32",
    "guint64",
    "glong",
    "gulong",
    "gfloat",
    "gdouble",
    "utf8",
    "filename",
    "GType"
)
/** Contains all the basic GObject data types. */
val basicDataTypes: Array<DataType> by lazy {
    val tmp = mutableListOf<DataType>()
    gBaseTypes.forEach { tmp += DataType(name = it) }
    tmp.toTypedArray()
}

data class DataType(val name: String, val namespace: String = "")

internal fun typeName(tag: GITypeTag, typeInfo: CPointer<GITypeInfo>): String = if (tag == GI_TYPE_TAG_INTERFACE) {
    val interfaceInfo = g_type_info_get_interface(typeInfo)
    val name = if (g_base_info_get_name(interfaceInfo)?.toKString() == "Object") ""
    else g_base_info_get_name(interfaceInfo)?.toKString()
    "${g_base_info_get_namespace(interfaceInfo)?.toKString()}$name"
} else {
    g_type_tag_to_string(tag)?.toKString() ?: ""
}

internal fun tagName(typeInfo: CPointer<GITypeInfo>): String {
    val tag = g_type_info_get_tag(typeInfo)
    return g_type_tag_to_string(tag)?.toKString() ?: ""
}

internal fun interfaceName(typeInfo: CPointer<GITypeInfo>): String = if (tagName(typeInfo) == "interface") {
    g_base_info_get_name(g_type_info_get_interface(typeInfo))?.toKString() ?: ""
} else {
    ""
}

internal fun interfaceNamespace(typeInfo: CPointer<GITypeInfo>): String = if (tagName(typeInfo) == "interface") {
    g_base_info_get_namespace(typeInfo)?.toKString() ?: ""
} else {
    ""
}